package contact.android.com.contact.ui.contactlist;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.lang.reflect.Type;
import java.util.List;

import contact.android.com.contact.BuildConfig;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.utils.CONST;
import contact.android.com.contact.utils.ContactListTestData;

import static junit.framework.TestCase.assertTrue;


/**
 * @author zhangzhenfang
 * @since 1/20/17 9:14 AM
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE, packageName = "contact.android.com.contact")
public class SortContactListInteractorTest {
    private SortContactListInteractor sortContactListInteractor;
    private List<Contact> contactListFull;
    private Gson gson = new Gson();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Type listType = new TypeToken<List<Contact>>(){}.getType();
        contactListFull = gson.fromJson(ContactListTestData.contactListFull, listType);
        sortContactListInteractor = new SortContactListInteractor();
    }

    @Test
    public void sortingContactListAsc() throws Exception {
            sortContactListInteractor.sortingContactList(CONST.SORTED_ORDER.asc, contactListFull);
            for(int i = 0; i < contactListFull.size()-1; ++i) {
                Contact cur = contactListFull.get(i);
                Contact next = contactListFull.get(i+1);
                assertTrue(cur.name.compareToIgnoreCase(next.name)<=0);
            }
    }

    @Test
    public void sortingContactListDes() throws Exception {
            sortContactListInteractor.sortingContactList(CONST.SORTED_ORDER.dec, contactListFull);
            for(int i = 0; i < contactListFull.size()-1; ++i) {
                Contact cur = contactListFull.get(i);
                Contact next = contactListFull.get(i+1);
                assertTrue(cur.name.compareToIgnoreCase(next.name)>0);
            }
    }

}