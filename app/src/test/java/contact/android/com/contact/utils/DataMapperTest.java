package contact.android.com.contact.utils;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import contact.android.com.contact.BuildConfig;
import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.Contact;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author zhangzhenfang
 * @since 1/20/17 10:18 AM
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE, packageName = "contact.android.com.contact")
public class DataMapperTest {
    private DataMapper dataMapper;
    private Gson gson;
    private Contact one;

    @Before
    public void setUp() throws Exception {
        gson = new Gson();
        one = gson.fromJson(ContactListTestData.contactListOne, Contact.class);
        dataMapper = new DataMapper(gson);
    }

    @Test
    public void map() throws Exception {
        DBContact dbContact = new DBContact();
        dataMapper.map(one, dbContact);

        assertThat(dbContact.getId()).isEqualTo(1);
        assertThat(dbContact.getName()).isEqualTo("Leanne Graham");
        //assertThat(dbContact.getUsername()).isEqualTo("Bret");
        //assertThat(dbContact.getEmail()).isEqualTo("Sincere@april.biz");
        assertThat(dbContact.getPhone()).isEqualTo("1-770-736-8031 x56442");
        //assertThat(dbContact.getWebsite()).isEqualTo("hildegard.org");
        //assertThat(dbContact.getAddress()).isEqualTo("{\"street\":\"Kulas Light\",\"suite\":\"Apt. 556\",\"city\":\"Gwenborough\",\"zipcode\":\"92998-3874\",\"geo\":{\"lat\":\"-37.3159\",\"lng\":\"81.1496\"}}");
        //assertThat(dbContact.getCompany()).isEqualTo("{\"name\":\"Romaguera-Crona\",\"catchPhrase\":\"Multi-layered client-server neural-net\",\"bs\":\"harness real-time e-markets\"}");
    }

    @Test
    public void map1() throws Exception {
        DBContact dbContact = new DBContact();
        dbContact.setId(1);
        dbContact.setName(("Leanne Graham"));
        //dbContact.setUsername("Bret");
        //dbContact.setEmail("Sincere@april.biz");
        dbContact.setPhone("1-770-736-8031 x56442");
        //dbContact.setWebsite(("hildegard.org"));
        //dbContact.setAddress("{\"street\":\"Kulas Light\",\"suite\":\"Apt. 556\",\"city\":\"Gwenborough\",\"zipcode\":\"92998-3874\",\"geo\":{\"lat\":\"-37.3159\",\"lng\":\"81.1496\"}}");
        //dbContact.setCompany("{\"name\":\"Romaguera-Crona\",\"catchPhrase\":\"Multi-layered client-server neural-net\",\"bs\":\"harness real-time e-markets\"}");

        Contact contact = dataMapper.map(dbContact);
        assertThat(contact.id).isEqualTo(one.id);
        assertThat(contact.name).isEqualTo(one.name);
        assertThat(contact.username).isEqualTo(one.username);
        assertThat(contact.email).isEqualTo(one.email);
        assertThat(contact.phone).isEqualTo(one.phone);
        assertThat(contact.website).isEqualTo(one.website);
        assertThat(contact.address).isEqualToIgnoringGivenFields(one.address, "geo");
        assertThat(contact.address.geo).isEqualToComparingFieldByField(one.address.geo);
        assertThat(contact.company).isEqualToComparingFieldByField(one.company);
    }

}