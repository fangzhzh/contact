package contact.android.com.contact.ui.contactdetail;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import contact.android.com.contact.BuildConfig;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.utils.ContactListTestData;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * @author zhangzhenfang
 * @since 1/20/17 8:51 AM
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE, packageName = "contact.android.com.contact")
public class ContactNewPresenterTest {
    private ContactDetailPresenter presenter;
    private Gson gson;
    private Contact one;

    @Mock
    ContactDetailInteractor contactDetailInteractor;
    @Mock
    ContactDetailView view;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        gson = new Gson();
        one = gson.fromJson(ContactListTestData.contactListOne, Contact.class);
        presenter = new ContactDetailPresenter(contactDetailInteractor);
        presenter.takeView(view);
    }

    @Test
    public void onDestroy() throws Exception {
        presenter.onDestroy();
        assertFalse(EventBus.getDefault().isRegistered(presenter));
    }

    @Test
    public void onInit() throws Exception {
        assertTrue(EventBus.getDefault().isRegistered(presenter));

    }

    @Test
    public void loadContact() throws Exception {
        presenter.loadContact(0);
        verify(contactDetailInteractor).getContact(0);
    }

    @Test
    public void onContactDetailLoadedWithNullContact() throws Exception {
        FetchContactDetailEventMessage message = new FetchContactDetailEventMessage(null);
        presenter.onContactDetailLoaded(message);
        verify(view, never()).contactLoaded(null);
    }

    @Test
    public void onContactDetailLoadedWithRealContact() throws Exception {
        FetchContactDetailEventMessage message = new FetchContactDetailEventMessage(one);
        presenter.onContactDetailLoaded(message);
        verify(view).contactLoaded(one);

    }
}