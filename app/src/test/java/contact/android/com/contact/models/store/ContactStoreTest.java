package contact.android.com.contact.models.store;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import contact.android.com.contact.BuildConfig;
import contact.android.com.contact.database.DatabaseManager;
import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.utils.ContactListTestData;
import contact.android.com.contact.utils.DataMapper;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author zhangzhenfang
 * @since 1/20/17 9:33 AM
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE, packageName = "contact.android.com.contact")
public class ContactStoreTest {
    private ContactStore contactStore;
    private Context context;
    private DatabaseManager databaseManager;
    private Contact one;
    private Gson gson;
    private DataMapper dataMapper;
    private List<Contact> contactListFull;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        context = RuntimeEnvironment.application;
        databaseManager = new DatabaseManager(context);
        contactStore = new ContactStore(databaseManager);
        gson = new Gson();
        one = gson.fromJson(ContactListTestData.contactListOne, Contact.class);
        dataMapper = new DataMapper(gson);
        Type listType = new TypeToken<List<Contact>>(){}.getType();
        contactListFull = gson.fromJson(ContactListTestData.contactListFull, listType);
    }

    ///////////////////////// no data //////////////////
    @Test
    public void getContactNoData() throws Exception {
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(1)).isNull();
        assertThat(contactStore.getContact(100)).isNull();
    }

    @Test
    public void getAllContactsNoData() throws Exception {
        assertThat(contactStore.getAllContacts().size()).isEqualTo(0);
    }

    @Test
    public void removeAllNoData() throws Exception {
        contactStore.removeAll();
        assertThat(contactStore.getAllContacts().size()).isEqualTo(0);
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(1)).isNull();
        assertThat(contactStore.getContact(100)).isNull();
    }

    @Test
    public void saveContactListNoData() throws Exception {
        contactStore.saveContactList(null);
        assertThat(contactStore.getAllContacts().size()).isEqualTo(0);
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(1)).isNull();
        assertThat(contactStore.getContact(100)).isNull();
    }

    ///////////////////////// one data //////////////////


    @Test
    public void saveOneData() throws Exception {

        // save
        DBContact dbContact = new DBContact();
        dataMapper.map(one, dbContact);
        contactStore.saveContact(dbContact);

        // test
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(one.id)).isNotNull();
        assertThat(contactStore.getAllContacts().size()).isEqualTo(1);

        // remove
        contactStore.removeAll();
        // test
        assertThat(contactStore.getAllContacts().size()).isEqualTo(0);
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(1)).isNull();
        assertThat(contactStore.getContact(100)).isNull();
    }

    ///////////////////////// list of data //////////////////

    @Test
    public void saveListOfData() throws Exception {
        List<DBContact> dbContactList = new ArrayList<>();
        for (Contact contact : contactListFull) {
            DBContact dbContact = new DBContact();
            dataMapper.map(contact, dbContact);
            dbContactList.add(dbContact);
        }

        contactStore.saveContactList(dbContactList);
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(1)).isNotNull();
        assertThat(contactStore.getContact(2)).isNotNull();
        assertThat(contactStore.getContact(3)).isNotNull();
        assertThat(contactStore.getContact(4)).isNotNull();
        assertThat(contactStore.getContact(5)).isNotNull();
        assertThat(contactStore.getContact(6)).isNotNull();
        assertThat(contactStore.getContact(7)).isNotNull();
        assertThat(contactStore.getContact(8)).isNotNull();
        assertThat(contactStore.getContact(9)).isNotNull();
        assertThat(contactStore.getContact(10)).isNotNull();
        assertThat(contactStore.getContact(11)).isNull();
        assertThat(contactStore.getAllContacts().size()).isEqualTo(10);


        // remove
        contactStore.removeAll();
        // test
        assertThat(contactStore.getAllContacts().size()).isEqualTo(0);
        assertThat(contactStore.getContact(0)).isNull();
        assertThat(contactStore.getContact(1)).isNull();
        assertThat(contactStore.getContact(100)).isNull();
    }
}