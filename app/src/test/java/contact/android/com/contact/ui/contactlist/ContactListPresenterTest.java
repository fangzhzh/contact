package contact.android.com.contact.ui.contactlist;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import contact.android.com.contact.BuildConfig;
import contact.android.com.contact.R;
import contact.android.com.contact.api.ContactsAPIService;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.store.ContactStore;
import contact.android.com.contact.utils.CONST;
import contact.android.com.contact.utils.ContactListTestData;
import contact.android.com.contact.utils.DataMapper;
import dagger.Lazy;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


/**
 * @author zhangzhenfang
 * @since 1/19/17 11:03 PM
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = Config.NONE, packageName = "contact.android.com.contact")
public class ContactListPresenterTest {

    //    @InjectMocks
    ContactListPresenter presenter;

    @Mock
    ContactListView view;

    @Mock
    SortContactListInteractor sortContactListInteractor;

    @Mock
    ContactListInteractor contactListInteractor;

    @Mock
    Lazy<ContactsAPIService> contactsAPIServiceLazy;

    @Mock
    Lazy<DataMapper> dataMapper;

    @Mock
    Lazy<ContactStore> contactStore;

    private List<Contact> contactListFull;
    private List<Contact> contactListEmpty;

    private Gson gson = new Gson();

    private Context context;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        context = RuntimeEnvironment.application;
        presenter = new ContactListPresenter(contactListInteractor, sortContactListInteractor);
        Type listType = new TypeToken<List<Contact>>(){}.getType();
        contactListFull = gson.fromJson(ContactListTestData.contactListFull, listType);
        contactListEmpty = gson.fromJson(ContactListTestData.contactListEmpty, listType);
        presenter.takeView(view);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void onInit() throws Exception {
        assertTrue(EventBus.getDefault().isRegistered(presenter));
    }

    @Test
    public void onDestroy() throws Exception {
        presenter.onDestroy();
        assertFalse(EventBus.getDefault().isRegistered(presenter));
    }

    @Test
    public void getContact() throws Exception {
        presenter.getContact();

        verify(view).startLoading();
        verify(contactListInteractor).getContacts();
    }


    ////////////////////// onContactLoaded begin /////////////////////////
    @Test
    public void onContactLoaded() throws Exception {
        FetchContactEventMessage eventMessage = null;
        presenter.onContactLoaded(eventMessage);
    }



    @Test
    public void onContactLoadedWithRandomErrorNullList() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(null, 400, "random");
        presenter.onContactLoaded(eventMessage);
        verify(view).onError(400, "random");
        verify(view).stopLoading();
        verify(sortContactListInteractor, never()).sortContacts(CONST.SORTED_ORDER.asc, null);
    }

    @Test
    public void onContactLoadedWithRandomErrorNullListErrorNonNullList() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(contactListEmpty, 400, "random");
        presenter.onContactLoaded(eventMessage);
        verify(view).onError(400, "random");
        verify(view).stopLoading();
        verify(sortContactListInteractor, never()).sortContacts(CONST.SORTED_ORDER.asc, null);
    }
    @Test
    public void onContactLoadedWithMinus1ErrorNullList() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(null, -1, "network");
        presenter.onContactLoaded(eventMessage);
        verify(view).onError(-1, R.string.ct_bad_network);
        verify(view).stopLoading();
        verify(sortContactListInteractor, never()).sortContacts(CONST.SORTED_ORDER.asc, null);
    }

    @Test
    public void onContactLoadedWithMinus1ErrorNullListErrorNonNullList() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(contactListEmpty, -1, "network");
        presenter.onContactLoaded(eventMessage);
        verify(view).onError(-1, R.string.ct_bad_network);
        verify(view).stopLoading();
        verify(sortContactListInteractor, never()).sortContacts(CONST.SORTED_ORDER.asc, null);
    }

    @Test
    public void onContactLoadedWithNoErrorEmptyData() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(contactListEmpty, 0, "");
        presenter.onContactLoaded(eventMessage);
        verify(sortContactListInteractor).sortContacts(CONST.SORTED_ORDER.asc, contactListEmpty);
    }
    @Test
    public void onContactLoadedWithNoErrorFullData() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(contactListFull, 0, "");
        presenter.onContactLoaded(eventMessage);
        verify(sortContactListInteractor).sortContacts(CONST.SORTED_ORDER.asc, contactListFull);
    }

    @Test
    public void onContactLoadedNoErrorNewArray() throws Exception {
        List<Contact> contactList = new ArrayList<>();
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(contactList, 0, "");
        presenter.onContactLoaded(eventMessage);
        verify(sortContactListInteractor).sortContacts(CONST.SORTED_ORDER.asc, contactList);
    }
    @Test
    public void onContactLoadedWithDirtyData() throws Exception {
        FetchContactEventMessage eventMessage = new FetchContactEventMessage(null, 0, "");
        presenter.onContactLoaded(eventMessage);
    }
    @Test
    public void onContactLoadedWithNull() throws Exception {
        presenter.onContactLoaded(null);
    }
    ////////////////////// onContactLoaded end /////////////////////////


    ///////////////////// sortContact begin  ////////////////////
    @Test
    public void sortContactHasData() throws Exception {
        presenter.setContactList(contactListFull);
        presenter.sortContact(CONST.SORTED_ORDER.asc);
        verify(sortContactListInteractor).sortContacts(CONST.SORTED_ORDER.asc, contactListFull);
    }
    @Test
    public void sortContactNoData() throws Exception {
        presenter.setContactList(null);
        presenter.sortContact(CONST.SORTED_ORDER.asc);
        verify(sortContactListInteractor, never()).sortContacts(CONST.SORTED_ORDER.asc, null);
    }
    ///////////////////// sortContact end  ////////////////////


    @Test
    public void onContactSortedWithNull() throws Exception {
        List<Contact> contactList = new ArrayList<>();
        SortContactEventMessage eventMessage = new SortContactEventMessage(contactList);
        presenter.onContactSorted(eventMessage);
        verify(view).contactLoaded(contactList);
        verify(view).stopLoading();
    }
    @Test
    public void onContactSortedWithData() throws Exception {
        SortContactEventMessage eventMessage = new SortContactEventMessage(contactListFull);
        presenter.onContactSorted(eventMessage);
        verify(view).contactLoaded(contactListFull);
        verify(view).stopLoading();
    }
}

