package contact.android.com.contact.application;


import com.facebook.stetho.Stetho;
import com.frogermcs.androiddevmetrics.AndroidDevMetrics;

import org.androidannotations.annotations.EApplication;

/**
 * @author zhangzhenfang
 * @since 1/19/17 1:45 PM
 */
@EApplication
public class ContactApplicationDebug extends ContactApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        setupDebugEnvironment();
    }

    private void setupDebugEnvironment() {
        AndroidDevMetrics.initWith(this);
        Stetho.initializeWithDefaults(this);
    }

}
