package contact.android.com.contact.models;

import contact.android.com.contact.utils.TimeUtil;

public class VMNote {

    private String note;

    private int timestamp;

    public VMNote() {
    }

    public VMNote(String note) {
        this.note = note;
        timestamp = TimeUtil.now();
    }

    public final String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public final int getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
