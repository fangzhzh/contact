package contact.android.com.contact.database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import java.sql.SQLException;

import contact.android.com.contact.database.DatabaseHelper;

public abstract class BaseInfoDao<D, T> {

    private Dao<D, T> mDao;

    private final DatabaseHelper mDBHelper;

    private Class<D> mClass;

    public BaseInfoDao(DatabaseHelper helper, Class<D> clazz) {
        mDBHelper = helper;
        mClass = clazz;
    }

    protected DatabaseHelper getHelper() {
        return mDBHelper;
    }

    protected Dao<D, T> getDao() throws SQLException {
        if (mDao == null) {
            mDao = DaoManager.createDao(getHelper().getConnectionSource(), getDBObjectClass());
            mDao.setObjectCache(true);
        }
        return mDao;
    }

    public Class getDBObjectClass() {
        return mClass;
    }

    public void clearCache() {
        if (mDao != null) {
            mDao.clearObjectCache();
        }
    }

}