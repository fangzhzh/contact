
package contact.android.com.contact.models;

import com.facebook.stetho.common.ListUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactWithNote {
    public ContactWithNote(int id, String name, String secondName,
                   String phone, String note) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.phone = phone;
        List<VMNote> vmNotes = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        TypeReference<List<VMNote>> mapType = new TypeReference<List<VMNote>>() {};
        try {
            vmNotes = objectMapper.readValue(note, mapType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(vmNotes, new Comparator<VMNote>() {
            @Override public int compare(VMNote o1, VMNote o2) {
                return o2.getTimestamp() - o1.getTimestamp();
            }
        });
        this.note = vmNotes;
    }


    public int id;
    public String name;
    public String secondName;
    public String phone;
    public List<VMNote> note;

}
