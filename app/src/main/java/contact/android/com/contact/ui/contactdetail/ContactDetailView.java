package contact.android.com.contact.ui.contactdetail;

import contact.android.com.contact.models.Contact;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:26 PM
 */
public interface ContactDetailView {
    void contactLoaded(Contact contact);
}
