package contact.android.com.contact.database.orm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "sp_Note") public class DBNote {
    @DatabaseField(columnName = COLUMN.NOTE_ID, generatedId = true) private int id;

    @DatabaseField(columnName = COLUMN.CONTACTIID) private int contactiid;

    @DatabaseField(columnName = COLUMN.NOTE) private String note;

    @DatabaseField(columnName = COLUMN.TIMESTAMP) private int timestamp;

    public DBNote() {
    }

    public final int getContactiid() {
        return this.contactiid;
    }

    public void setContactiid(int contactIId) {
        this.contactiid = contactIId;
    }

    public final String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public final int getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public static String createContactiid() {
        return "alter table sp_Note ADD contactiid INTEGER;";
    }

    public static String createNote() {
        return "alter table sp_Note ADD note VARCHAR;";
    }

    public static String createTimestamp() {
        return "alter table sp_Note ADD timestamp INTEGER;";
    }

    public interface COLUMN {
        String CONTACTIID = "contactiid";
        String NOTE = "note";
        String TIMESTAMP = "timestamp";
        String NOTE_ID = "id";
    }
}
