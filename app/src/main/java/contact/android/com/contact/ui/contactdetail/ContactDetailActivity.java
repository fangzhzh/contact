package contact.android.com.contact.ui.contactdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import contact.android.com.contact.R;
import contact.android.com.contact.activity.ActivityModule;
import contact.android.com.contact.application.ContactApplication;
import contact.android.com.contact.models.Address;
import contact.android.com.contact.models.Company;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.ui.base.BaseActivity;
import contact.android.com.contact.ui.base.Scope;

@EActivity(R.layout.activity_contact_detail)
public class ContactDetailActivity extends BaseActivity implements ContactDetailView{

    @Extra("contactId")
    int contactId;

    @Inject
    ContactDetailPresenter presenter;
    @Inject
    Scope scope;


    @ViewById(R.id.contact_detail_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.name)
    TextView nameView;
    @ViewById(R.id.phone)
    TextView phoneView;
    @ViewById(R.id.address)
    TextView addressView;
    @ViewById(R.id.website)
    TextView webSiteView;
    @ViewById(R.id.company)
    TextView companyView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void onViewInit() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        scope.attach(presenter);
        presenter.takeView(this);
        presenter.loadContact(contactId);

    }

    @Override
    protected void setupActivityComponent() {
        ContactApplication.get().getAppComponent().plus(new ActivityModule(this)).inject(this);
    }

    @Override
    public void contactLoaded(Contact contact) {
        phoneView.setText(contact.phone);
        toolbar.setTitle(contact.name);
    }


}
