package contact.android.com.contact.database.dao;

import com.j256.ormlite.dao.Dao;
import contact.android.com.contact.database.DatabaseHelper;
import contact.android.com.contact.database.orm.DBNote;
import contact.android.com.contact.utils.BBAppLogger;
import java.lang.Object;
import java.util.List;
import java.util.concurrent.Callable;

public class NoteDao extends BaseInfoDao<DBNote, Object> {
  public NoteDao(DatabaseHelper helper) {
    super(helper, DBNote.class);
  }

  public DBNote get(int id) {
    try {
      final Dao<DBNote, Object> dao = getDao();
      return dao.queryForId(id);
    }
    catch(Exception e) {
      BBAppLogger.e(e);
    }
    return null;
  }


  public void save(final DBNote dbObject) {
    try {
      final Dao<DBNote, java.lang.Object> dao = getDao();
      dao.createOrUpdate(dbObject);
    }
    catch(Exception e) {
      BBAppLogger.e(e);
    }
  }

  public void save(final List<DBNote> dbObjects) {
    if ( dbObjects.size() <= 0) {
      return;
    }
    try {
      final Dao<DBNote, java.lang.Object> dao = getDao();
      dao.callBatchTasks(new Callable<Object>() {
      	@Override
      	public Object call() throws Exception  {
        	for (DBNote dbObject : dbObjects)  {
          	dao.createOrUpdate(dbObject);
        }
        	return null;
      }
    } );
    } catch(Exception e) {
      BBAppLogger.e(e);
    }
  }
}
