package contact.android.com.contact.database.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import contact.android.com.contact.database.DatabaseHelper;
import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.utils.BBAppLogger;
import contact.android.com.contact.utils.ListUtils;


public class ContactDao extends BaseInfoDao<DBContact, Integer> {
  public ContactDao(DatabaseHelper helper) {
    super(helper, DBContact.class);
  }

  public DBContact get(int id) {
    try {
      final Dao<DBContact, Integer> dao = getDao();
      return dao.queryForId(id);
    }
    catch(Exception e) {
      BBAppLogger.e(e);
    }
    return null;
  }

  public List<DBContact> getList(List<Object> idList) {
    try {
      final Dao<DBContact, Integer> dao = getDao();
      final QueryBuilder<DBContact, Integer> builder = dao.queryBuilder();
      builder.where().in(DBContact.COLUMN.ID, idList);
      return builder.query();
    }
    catch(Exception e) {
      BBAppLogger.e(e);
    }
    return new ArrayList<>();
  }

  public void save(final DBContact dbObject) {
    try {
      final Dao<DBContact, Integer> dao = getDao();
      dao.createOrUpdate(dbObject);
    }
    catch(Exception e) {
      BBAppLogger.e(e);
    }
  }

  public void save(final List<DBContact> dbObjects) {
    if (ListUtils.isEmpty(dbObjects)) {
      return;
    }
    try {
      final Dao<DBContact, Integer> dao = getDao();
      dao.callBatchTasks(new Callable<Object>() {
      	@Override
      	public Object call() throws Exception  {
        	for (DBContact dbObject : dbObjects)  {
          	dao.createOrUpdate(dbObject);
        }
        	return null;
      }
    } );
    } catch(Exception e) {
      BBAppLogger.e(e);
    }
  }

    public List<DBContact> getAll() {
        try {
            final Dao<DBContact, Integer> dao = getDao();
            return dao.queryForAll();
        }
        catch(Exception e) {
            BBAppLogger.e(e);
        }
        return new ArrayList<>();
    }

    public void removeAll() {
        try {
            final Dao<DBContact, Integer> dao = getDao();
            dao.deleteBuilder().delete();
        }
        catch(Exception e) {
            BBAppLogger.e(e);
        }
    }
}
