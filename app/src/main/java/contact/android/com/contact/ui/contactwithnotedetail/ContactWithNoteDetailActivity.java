package contact.android.com.contact.ui.contactwithnotedetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import contact.android.com.contact.models.ContactWithNote;
import contact.android.com.contact.models.VMNote;
import contact.android.com.contact.utils.TimeUtil;
import java.util.List;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import contact.android.com.contact.R;
import contact.android.com.contact.activity.ActivityModule;
import contact.android.com.contact.application.ContactApplication;
import contact.android.com.contact.ui.base.BaseActivity;
import contact.android.com.contact.ui.base.Scope;

@EActivity(R.layout.activity_contact_with_note_detail)
public class ContactWithNoteDetailActivity extends BaseActivity implements
    ContactWithNoteDetailView {

    @Extra("contactId")
    int contactId;

    @Inject ContactWithNoteDetailPresenter presenter;
    @Inject
    Scope scope;


    @ViewById(R.id.contact_detail_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.name)
    TextView nameView;
    @ViewById(R.id.phone)
    TextView phoneView;
    @ViewById(R.id.note)
    TextView noteView;

    @ViewById(R.id.new_note) EditText newNoteView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void onViewInit() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        scope.attach(presenter);
        presenter.takeView(this);
        presenter.loadContact(contactId);
    }

    @Override
    protected void setupActivityComponent() {
        ContactApplication.get().getAppComponent().plus(new ActivityModule(this)).inject(this);
    }

    @Override
    public void contactLoaded(ContactWithNote contact) {
        nameView.setText(contact.name + ", " + contact.secondName);
        phoneView.setText(contact.phone);
        setNoteView(contact.note);
        toolbar.setTitle(contact.name);
    }

    private void setNoteView(List<VMNote> note) {
        StringBuilder builder = new StringBuilder();
        for (VMNote vmNote : note) {
            builder.append(vmNote.getNote() + " @ " + TimeUtil.getReadableDate(vmNote.getTimestamp()));
            builder.append("\n");
        }
        noteView.setText(builder.toString());
    }

    @Click(R.id.new_note_save_button)
    void onClickSaveNewNote() {
        presenter.saveNewNote(contactId, newNoteView.getText().toString());
        newNoteView.setText("");
    }


}
