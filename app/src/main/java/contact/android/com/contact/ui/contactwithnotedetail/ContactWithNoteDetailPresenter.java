package contact.android.com.contact.ui.contactwithnotedetail;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import contact.android.com.contact.ui.base.IScreenPresenter;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:25 PM
 */
public class ContactWithNoteDetailPresenter extends IScreenPresenter<ContactWithNoteDetailView>{
    private ContactWithNoteDetailInteractor contactWithNoteDetailInteractor;
    private ContactSaveNewNoteInteractor contactSaveNewNoteInteractor;

    @Inject
    public ContactWithNoteDetailPresenter(ContactWithNoteDetailInteractor contactWithNoteDetailInteractor,
        ContactSaveNewNoteInteractor contactSaveNewNoteInteractor) {
        this.contactWithNoteDetailInteractor = contactWithNoteDetailInteractor;
        this.contactSaveNewNoteInteractor = contactSaveNewNoteInteractor;
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onInit() {
        super.onInit();
        EventBus.getDefault().register(this);
    }

    void loadContact(int id) {
        contactWithNoteDetailInteractor.getContact(id);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactDetailLoaded(@NonNull FetchContactWithNoteDetailEventMessage eventMessage) {
        if (eventMessage.contact != null) {
            view.contactLoaded(eventMessage.contact);
        }
    }

    public void saveNewNote(int contactId, String newNote) {
        contactSaveNewNoteInteractor.saveNewNote(contactId, newNote);
    }
}
