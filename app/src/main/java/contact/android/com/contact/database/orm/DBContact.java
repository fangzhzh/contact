package contact.android.com.contact.database.orm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import contact.android.com.contact.models.VMNote;
import contact.android.com.contact.models.VMNotes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

@DatabaseTable(
        tableName = "sp_Contact")
public class DBContact {
    @DatabaseField(
            columnName = COLUMN.ID, generatedId = true)
    private long id;

    @DatabaseField(
            columnName = COLUMN.NAME)
    private String name;

    @DatabaseField(
            columnName = COLUMN.SECOND_NAME)
    private String secondName;

    @DatabaseField(
            columnName = COLUMN.PHONE)
    private String phone;

    @DatabaseField(
            columnName = COLUMN.NOTE)
    private String note;

    public DBContact() {
    }

    public DBContact(String firstName, String secondName, String phone, String note) {
        name = firstName;
        this.secondName = secondName;
        this.phone = phone;
        this.note = makeNote(note);
    }

    private String makeNote(String note) {
        VMNote vmNote = new VMNote(note);
        List<VMNote> vmNotes = new ArrayList<>();
        vmNotes.add(vmNote);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = null;
        try {
            json = objectMapper.writeValueAsString(vmNotes);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }


    public final int getId() {
        return (int)this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public final String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public final String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void addNewNote(String newNote) {
        List<VMNote> vmNotes = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        TypeReference<List<VMNote>> mapType = new TypeReference<List<VMNote>>() {};
        try {
            vmNotes = objectMapper.readValue(note, mapType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        VMNote vmNote = new VMNote(newNote);
        vmNotes.add(vmNote);

        String json = null;
        try {
            json = objectMapper.writeValueAsString(vmNotes);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        note = json;
    }

    public interface COLUMN {
        String ID = "id";

        String NAME = "name";

        String SECOND_NAME = "secondName";

        String PHONE = "phone";

        String NOTE = "note";
    }
}
