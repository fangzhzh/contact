package contact.android.com.contact.ui.contactwithnotedetail;

import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.ContactWithNote;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:35 PM
 */
public class FetchContactWithNoteDetailEventMessage {
    public final ContactWithNote contact;

    public FetchContactWithNoteDetailEventMessage(ContactWithNote contact) {
        this.contact = contact;
    }
}
