package contact.android.com.contact.ui.contactwithnotedetail;

import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.ContactWithNote;
import contact.android.com.contact.models.store.ContactStore;
import contact.android.com.contact.ui.base.BaseInteractor;
import contact.android.com.contact.utils.DataMapper;
import javax.inject.Inject;
import org.greenrobot.eventbus.EventBus;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:29 PM
 */
public class ContactSaveNewNoteInteractor extends BaseInteractor<ContactSaveNewNoteInteractor.ContactSaveNewNoteData> {

    private ContactStore contactStore;
    private DataMapper dataMapper;

    @Inject
    public ContactSaveNewNoteInteractor(ContactStore contactStore,
                                   DataMapper dataMapper) {
        this.contactStore = contactStore;
        this.dataMapper = dataMapper;
    }

    @Override
    protected void onExecute(ContactSaveNewNoteData data) {
        DBContact dbContact = contactStore.getContact(data.contactId);
        dbContact.addNewNote(data.newNote);
        contactStore.saveContact(dbContact);
        ContactWithNote contact = dataMapper.mapContactWithNote(dbContact);
        EventBus.getDefault().post(new FetchContactWithNoteDetailEventMessage(contact));
    }

    public void saveNewNote(int contactId, String newNote) {
        ContactSaveNewNoteData contactSaveNewNoteData = new ContactSaveNewNoteData(contactId, newNote);
        onExecute(contactSaveNewNoteData);
    }

    public static class ContactSaveNewNoteData extends BaseInteractor.Data{
        public final int contactId;
        private final String newNote;

        public ContactSaveNewNoteData(int contactId, String newNote) {
            super("ContactSaveNewNoteData"+contactId, "ContactSaveNewNoteData"+contactId);
            this.contactId = contactId;
            this.newNote = newNote;
        }
    }
}
