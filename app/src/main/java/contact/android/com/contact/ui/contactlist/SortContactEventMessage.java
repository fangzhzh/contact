package contact.android.com.contact.ui.contactlist;

import contact.android.com.contact.models.ContactWithNote;
import java.util.List;

import contact.android.com.contact.models.Contact;

/**
 * @author zhangzhenfang
 * @since 1/19/17 11:14 AM
 */
public class SortContactEventMessage {
    final public List<ContactWithNote> contactList;

    SortContactEventMessage(List<ContactWithNote> contactList) {
        this.contactList = contactList;
    }

}
