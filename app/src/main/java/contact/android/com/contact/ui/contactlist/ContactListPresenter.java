package contact.android.com.contact.ui.contactlist;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import contact.android.com.contact.models.ContactWithNote;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import contact.android.com.contact.R;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.ui.base.IScreenPresenter;
import contact.android.com.contact.utils.CONST;
import contact.android.com.contact.utils.ListUtils;
import hugo.weaving.DebugLog;

/**
 * @author zhangzhenfang
 * @since 1/18/17 2:03 PM
 */
@DebugLog
public class ContactListPresenter extends IScreenPresenter<ContactListView> {

    private ContactListInteractor contactListInteractor;
    private SortContactListInteractor sortContactListInteractor;
    List<ContactWithNote> contactList;
    private int order = CONST.SORTED_ORDER.note;

    @Inject
    public ContactListPresenter(ContactListInteractor contactListInteractor,
                                SortContactListInteractor sortContactListInteractor) {
        this.contactListInteractor = contactListInteractor;
        this.sortContactListInteractor = sortContactListInteractor;
    }

    @Override
    protected void onInit() {
        super.onInit();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    void getContact() {
        // get from api
        view.startLoading();
        loadFromServer();
    }

    private void loadFromServer() {
        contactListInteractor.getContacts();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactLoaded(FetchContactEventMessage eventMessage) {
        if (eventMessage == null
                ||
                (eventMessage.errCode == 0
                        && TextUtils.isEmpty(eventMessage.errMsg)
                        && eventMessage.contactList == null)) {
            return;
        }
        if (eventMessage.errCode != 0 || !TextUtils.isEmpty(eventMessage.errMsg)) {

            // first time,
            if (!ListUtils.isEmpty(this.contactList)) {
                return;
            }

            setContactList(eventMessage.contactList);
            if (eventMessage.errCode == -1) {
                view.stopLoading();
                view.onError(eventMessage.errCode,
                        R.string.ct_bad_network);
            } else {
                view.stopLoading();
                view.onError(eventMessage.errCode, eventMessage.errMsg);
            }
        } else {
            setContactList(eventMessage.contactList);
            sortContact(this.order, eventMessage.contactList);
        }
    }

    void setContactList(List<ContactWithNote> contactList) {
        this.contactList = contactList;
    }
    void sortContact(int order) {
        if (!ListUtils.isEmpty(this.contactList)) {
            this.order = order;
            sortContact(order, this.contactList);
        }
    }

    private void sortContact(int order, @NonNull List<ContactWithNote> contactList) {
        this.order = order;
        sortContactListInteractor.sortContacts(order, contactList);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactSorted(@NonNull SortContactEventMessage eventMessage) {
        loadContact(eventMessage.contactList);
    }

    private void loadContact(@NonNull List<ContactWithNote> contactList) {
        view.stopLoading();
        view.contactLoaded(contactList);

    }


}
