package contact.android.com.contact.ui.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangzhenfang
 * @since 1/18/17 6:53 AM
 */
public abstract class SimpleAdapter<T> extends BaseAdapter {
    List<T> itemList;

    protected SimpleAdapter() {
        itemList = new ArrayList<>();
    }

    public void setDataSource(List<T> itemList) {
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public T getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IListItemView itemView;
        if (convertView == null) {
            itemView = createItemView(parent.getContext(), position);
        } else {
            itemView = (IListItemView) convertView;
        }
        itemView.bind(getItem(position));
        return (View) itemView;
    }

    protected abstract IListItemView<T> createItemView(Context context, int position);
}
