package contact.android.com.contact.application;

import android.app.Application;
import android.content.Context;


import org.androidannotations.annotations.EApplication;

import contact.android.com.contact.BuildConfig;


/**
 * @author zhangzhenfang
 * @since 17/10/16 9:40 PM
 */
@EApplication
public class ContactApplication extends Application {
    public AppComponent appComponent;
    private static Application mInstance;

    public static Context getApplication() {
        return mInstance;
    }


    public static ContactApplication get(Context context) {
        return (ContactApplication) context.getApplicationContext();
    }


    public static ContactApplication get() {
        return (ContactApplication) mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initAppComponent();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
