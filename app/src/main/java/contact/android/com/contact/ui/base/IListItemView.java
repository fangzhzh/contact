package contact.android.com.contact.ui.base;

/**
 * @author zhangzhenfang
 * @since 1/18/17 6:56 AM
 */
public interface IListItemView<T> {
    void bind(T data);
}
