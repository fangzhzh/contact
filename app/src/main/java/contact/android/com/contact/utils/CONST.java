package contact.android.com.contact.utils;

/**
 * @author zhangzhenfang
 * @since 1/18/17 3:35 PM
 */
public class CONST {
    public static final String BASE_URL = "http://jsonplaceholder.typicode.com/";
    public static final String LOG_TAG = "contact";
    public static final int CONTACT_NEW_REQUEST_CODE = 1001;

    public interface SORTED_ORDER {
        int random = 0;
        int asc = 1;
        int dec = 2;
        int note = 3;
    }
}
