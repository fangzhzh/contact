package contact.android.com.contact.ui.contactnew;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import android.widget.TextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.METValidator;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

import contact.android.com.contact.R;
import contact.android.com.contact.activity.ActivityModule;
import contact.android.com.contact.application.ContactApplication;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.ui.base.BaseActivity;
import contact.android.com.contact.ui.base.Scope;

/**
 * @author zhangzf
 * @since Apr 12, 2017 7:47 PM
 */

@EActivity(R.layout.activity_contact_new)
public class ContactNewActivity extends BaseActivity implements ContactNewView {

    @ViewById(R.id.new_first_name) MaterialEditText firstNameView;
    @ViewById(R.id.new_second_name) MaterialEditText secondNameView;
    @ViewById(R.id.new_phone) MaterialEditText phoneView;
    @ViewById(R.id.new_note) MaterialEditText noteView;
    @ViewById(R.id.new_note_show) TextView noteShowView;
    @ViewById(R.id.contact_detail_toolbar)
    Toolbar toolbar;


    @Inject ContactNewPresenter presenter;
    @Inject Scope scope;
    @Inject Resources resources;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void onViewInit() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        scope.attach(presenter);
        presenter.takeView(this);
        firstNameView.addValidator(new EmptyValidator(resources.getString(R.string.ct_error_empty_first_name)));
        secondNameView.addValidator(new EmptyValidator(resources.getString(R.string.ct_error_empty_second_name)));
    }

    @Override
    protected void setupActivityComponent() {
        ContactApplication.get().getAppComponent().plus(new ActivityModule(this)).inject(this);
    }

    @Override
    public void contactSaved() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Click(R.id.contact_new_save)
    void onClickSave() {
        if (!validate()) {
            return;
        }
        presenter.saveContactNew(firstNameView.getText().toString(),
            secondNameView.getText().toString(),
            phoneView.getText().toString(),
            noteView.getText().toString());
    }

    private boolean validate() {
        return firstNameView.validate() && secondNameView.validate();
    }




    // empty validator
    private static class EmptyValidator extends METValidator {

        public EmptyValidator(@NonNull String errorMessage) {
            super(errorMessage);
        }

        @Override
        public boolean isValid(@NonNull CharSequence charSequence, boolean b) {
            return !TextUtils.isEmpty(charSequence);
        }
    }
}
