package contact.android.com.contact.utils;

import android.content.Context;
import contact.android.com.contact.application.ContactApplication;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class TimeUtil {

    public static int now() {
        long t = System.currentTimeMillis();
        return (int) (t / 1000);
    }

    public static String getReadableDate(int ts) {
        Context context = ContactApplication.getApplication();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH);
        return dateFormat.format(ts * 1000L);
    }
}