package contact.android.com.contact.application;

import javax.inject.Singleton;

import contact.android.com.contact.activity.ActivityComponent;
import contact.android.com.contact.activity.ActivityModule;
import dagger.Component;

/**
 * @author zhangzhenfang
 * @since 1/18/17 4:33 AM
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
        }
)
public interface AppComponent {
    ActivityComponent plus(ActivityModule activityModule);
}
