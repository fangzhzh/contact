package contact.android.com.contact.ui.contactwithnotedetail;

import contact.android.com.contact.models.ContactWithNote;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.store.ContactStore;
import contact.android.com.contact.ui.base.BaseInteractor;
import contact.android.com.contact.utils.DataMapper;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:29 PM
 */
public class ContactWithNoteDetailInteractor extends BaseInteractor<ContactWithNoteDetailInteractor.ContactWithNoteDetailData>{

    private ContactStore contactStore;
    private DataMapper dataMapper;

    @Inject
    public ContactWithNoteDetailInteractor(ContactStore contactStore,
                                   DataMapper dataMapper) {
        this.contactStore = contactStore;
        this.dataMapper = dataMapper;
    }
    public void getContact(int id) {
        execute(new ContactWithNoteDetailData(id));
    }

    @Override
    protected void onExecute(ContactWithNoteDetailData data) {
        DBContact dbContact = contactStore.getContact(data.contactId);
        ContactWithNote contact = dataMapper.mapContactWithNote(dbContact);
        EventBus.getDefault().post(new FetchContactWithNoteDetailEventMessage(contact));
    }


    public static class ContactWithNoteDetailData extends BaseInteractor.Data{
        public final int contactId;

        public ContactWithNoteDetailData(int contactId) {
            super("ContactWithNoteDetailInteractor"+contactId, "ContactWithNoteDetailInteractor"+contactId);
            this.contactId = contactId;
        }
    }
}
