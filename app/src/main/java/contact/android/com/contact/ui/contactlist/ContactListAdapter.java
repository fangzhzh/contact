package contact.android.com.contact.ui.contactlist;


import android.content.Context;

import javax.inject.Inject;

import contact.android.com.contact.models.Contact;
import contact.android.com.contact.ui.base.IListItemView;
import contact.android.com.contact.ui.base.SimpleAdapter;

/**
 * @author zhangzhenfang
 * @since 1/18/17 6:19 AM
 */
public class ContactListAdapter extends SimpleAdapter<Contact> {

    @Inject
    public ContactListAdapter() {
    }


    @Override
    protected IListItemView createItemView(Context context, int position) {
        return ContactListItemView_.build(context);
    }
}
