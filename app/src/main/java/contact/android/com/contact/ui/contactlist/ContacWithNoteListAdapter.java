package contact.android.com.contact.ui.contactlist;

import android.content.Context;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.ContactWithNote;
import contact.android.com.contact.ui.base.IListItemView;
import contact.android.com.contact.ui.base.SimpleAdapter;
import javax.inject.Inject;

/**
 * @author zhangzhenfang
 * @since 1/18/17 6:19 AM
 */
public class ContacWithNoteListAdapter extends SimpleAdapter<ContactWithNote> {

    @Inject
    public ContacWithNoteListAdapter() {
    }


    @Override
    protected IListItemView createItemView(Context context, int position) {
        return ContactWithNoteListItemView_.build(context);
    }
}
