package contact.android.com.contact.ui.contactlist;

import android.text.TextUtils;

import com.facebook.stetho.common.ListUtil;
import contact.android.com.contact.models.ContactWithNote;
import contact.android.com.contact.utils.ListUtils;
import org.greenrobot.eventbus.EventBus;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import contact.android.com.contact.models.Contact;
import contact.android.com.contact.ui.base.BaseInteractor;
import contact.android.com.contact.utils.CONST;

/**
 * @author zhangzhenfang
 * @since 1/19/17 10:58 AM
 */
public class SortContactListInteractor extends BaseInteractor<SortContactListInteractor.SortContactListData> {


    @Inject
    public SortContactListInteractor() {

    }

    public void sortContacts(int order, List<ContactWithNote> contactList) {
        SortContactListData data = new SortContactListData(order, contactList);
        onExecute(data);
    }

    @Override
    protected void onExecute(final SortContactListData data) {
        sortingContactList(data.order, data.contactList);
        EventBus.getDefault().post(new SortContactEventMessage(data.contactList));
    }

    void sortingContactList(final int order, List<ContactWithNote> contactList) {
        if (order == CONST.SORTED_ORDER.note) {
            Collections.sort(contactList, new Comparator<ContactWithNote>() {
                @Override
                public int compare(ContactWithNote o1, ContactWithNote o2) {
                    if (!ListUtils.isEmpty(o1.note) && !ListUtils.isEmpty(o2.note)) {
                        return o2.note.get(0).getTimestamp() - o1.note.get(0).getTimestamp();
                    } else if (ListUtils.isEmpty(o1.note)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
        } else {
            Collections.sort(contactList, new Comparator<ContactWithNote>() {
                @Override
                public int compare(ContactWithNote o1, ContactWithNote o2) {
                    int factor = (order == CONST.SORTED_ORDER.asc ? 1 : -1);
                    if (TextUtils.isEmpty(o2.name)) {
                        return factor;
                    } else if (TextUtils.isEmpty(o1.name)) {
                        return  -1 * factor;
                    }

                    return o1.name.compareToIgnoreCase(o2.name) * factor;
                }
            });
        }

    }

    static class SortContactListData extends BaseInteractor.Data {
        private int order;
        private List<ContactWithNote> contactList;

        SortContactListData(int order, List<ContactWithNote> contactList) {
            super("SortContactListInteractor"+order, "SortContactListInteractor"+order);
            this.order = order;
            this.contactList = contactList;
        }
    }

}
