package contact.android.com.contact.ui.contactdetail;

import contact.android.com.contact.models.Contact;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:35 PM
 */
public class FetchContactDetailEventMessage {
    public final Contact contact;

    public FetchContactDetailEventMessage(Contact contact) {
        this.contact = contact;
    }
}
