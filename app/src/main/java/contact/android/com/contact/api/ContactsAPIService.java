package contact.android.com.contact.api;

import java.util.List;

import contact.android.com.contact.models.Contact;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author zhangzhenfang
 * @since 1/18/17 2:56 PM
 */
public interface ContactsAPIService {
    @GET("users")
    Call<List<Contact>> getContacts();

}
