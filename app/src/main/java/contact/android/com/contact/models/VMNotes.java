package contact.android.com.contact.models;

import java.util.ArrayList;
import java.util.List;

public class VMNotes {
    List<VMNote> notes;

    public VMNotes(List<VMNote> notes) {
        this.notes = notes;
    }

    public VMNotes() {
        notes = new ArrayList<>();
    }

    public void addNewNote(VMNote vmNote) {
        notes.add(vmNote);
    }
}
