package contact.android.com.contact.ui.contactnew;

import android.support.annotation.NonNull;

import android.text.Editable;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import contact.android.com.contact.ui.base.IScreenPresenter;

/**
 * @author zhangzf
 * @since Apr 12, 2017 7:34 PM
 */
public class ContactNewPresenter extends IScreenPresenter<ContactNewView>{
    private ContactNewInteractor contactNewInteractor;

    @Inject
    public ContactNewPresenter(ContactNewInteractor contactNewInteractor) {
        this.contactNewInteractor = contactNewInteractor;
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onInit() {
        super.onInit();
        EventBus.getDefault().register(this);
    }

    void loadContact(int id) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactDetailLoaded(@NonNull FetchContactNewEventMessage eventMessage) {
        view.contactSaved();
    }

    public void saveContactNew(String firstName, String secondName, String phone, String note) {
        contactNewInteractor.saveNewContact(firstName, secondName, phone, note);
    }
}
