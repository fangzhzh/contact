package contact.android.com.contact.ui.contactnew;


/**
 * @author zhangzf
 * @since Apr 12, 2017 7:34 PM
 */
public interface ContactNewView {
    void contactSaved();
}
