package contact.android.com.contact.utils;

import java.util.List;

/**
 * @author zhangzhenfang
 * @since 1/18/17 2:34 PM
 */
public class ListUtils {
    public static <T> boolean isEmpty(List<T> list) {
        return list == null || list.isEmpty();
    }

}
