package contact.android.com.contact.utils;

import android.util.Log;

/**
 * @author zhangzhenfang
 * @since 1/19/17 6:57 PM
 */
public class BBAppLogger {
    public static void e(Exception e) {
        Log.e(CONST.LOG_TAG, e.toString());
    }
}
