package contact.android.com.contact.ui.contactdetail;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.store.ContactStore;
import contact.android.com.contact.ui.base.BaseInteractor;
import contact.android.com.contact.utils.DataMapper;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:29 PM
 */
public class ContactDetailInteractor extends BaseInteractor<ContactDetailInteractor.ContactDetailData>{

    private ContactStore contactStore;
    private DataMapper dataMapper;

    @Inject
    public ContactDetailInteractor(ContactStore contactStore,
                                   DataMapper dataMapper) {
        this.contactStore = contactStore;
        this.dataMapper = dataMapper;
    }
    public void getContact(int id) {
        execute(new ContactDetailData(id));
    }

    @Override
    protected void onExecute(ContactDetailData data) {
        DBContact dbContact = contactStore.getContact(data.contactId);
        Contact contact = dataMapper.map(dbContact);
        EventBus.getDefault().post(new FetchContactDetailEventMessage(contact));
    }


    public static class ContactDetailData extends BaseInteractor.Data{
        public final int contactId;

        public ContactDetailData(int contactId) {
            super("ContactDetailInteractor"+contactId, "ContactDetailInteractor"+contactId);
            this.contactId = contactId;
        }
    }
}
