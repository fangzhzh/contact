package contact.android.com.contact.ui.contactnew;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import contact.android.com.contact.database.orm.DBNote;
import contact.android.com.contact.models.VMNote;
import contact.android.com.contact.models.VMNotes;
import java.util.Arrays;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.store.ContactStore;
import contact.android.com.contact.ui.base.BaseInteractor;
import contact.android.com.contact.utils.DataMapper;

/**
 * @author zhangzf
 * @since Apr 12, 2017 7:34 PM
 */
public class ContactNewInteractor extends BaseInteractor<ContactNewInteractor.ContactNewData>{

    private ContactStore contactStore;

    @Inject
    public ContactNewInteractor(ContactStore contactStore) {
        this.contactStore = contactStore;
    }

    @Override
    protected void onExecute(ContactNewData data) {
        DBContact dbContact = new DBContact(data.firstName, data.secondName, data.phone, data.note);
        contactStore.saveContact(dbContact);
        EventBus.getDefault().post(new FetchContactNewEventMessage());
    }

    protected void saveNewContact(String firstName, String secondName, String phone, String note) {
        ContactNewData contactNewData = new ContactNewData(firstName, secondName, phone, note);
        onExecute(contactNewData);
    }

    public static class ContactNewData extends BaseInteractor.Data{

        public final String firstName;
        public final String secondName;
        public final String phone;
        public final String note;

        public ContactNewData(String firstName, String secondName, String phone, String note) {
            super("ContactNewInteractor:"+firstName + " " + secondName + " " + phone + " " + note ,
                "ContactNewInteractor:"+firstName + " " + secondName + " " + phone + " " + note);
            this.firstName = firstName;
            this.secondName = secondName;
            this.phone = phone;
            this.note = note;
        }
    }
}
