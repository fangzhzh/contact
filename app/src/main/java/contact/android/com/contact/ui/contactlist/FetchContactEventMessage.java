package contact.android.com.contact.ui.contactlist;

import contact.android.com.contact.models.ContactWithNote;
import java.util.List;

import contact.android.com.contact.models.Contact;

/**
 * @author zhangzhenfang
 * @since 1/18/17 7:12 PM
 */
public class FetchContactEventMessage {
    public final List<ContactWithNote> contactList;
    public final int errCode;
    public final String errMsg;

    FetchContactEventMessage(List<ContactWithNote> contactList, int errCode, String errMsg) {
        this.contactList = contactList;
        this.errCode = errCode;
        this.errMsg = errMsg;
    }
}
