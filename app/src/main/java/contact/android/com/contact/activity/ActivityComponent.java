package contact.android.com.contact.activity;

import contact.android.com.contact.ui.contactdetail.ContactDetailActivity;
import contact.android.com.contact.ui.contactlist.ContactListActivity;
import contact.android.com.contact.ui.contactnew.ContactNewActivity;
import contact.android.com.contact.ui.contactwithnotedetail.ContactWithNoteDetailActivity;
import dagger.Subcomponent;

/**
 * @author zhangzhenfang
 * @since 1/18/17 4:33 AM
 */


@ActivityScope
@Subcomponent(
        modules = ActivityModule.class
)


public interface ActivityComponent {
    void inject(ContactListActivity activity);
    void inject(ContactDetailActivity activity);
    void inject(ContactNewActivity activity);
    void inject(ContactWithNoteDetailActivity activity);
}
