package contact.android.com.contact.ui.contactdetail;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import contact.android.com.contact.ui.base.IScreenPresenter;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:25 PM
 */
public class ContactDetailPresenter extends IScreenPresenter<ContactDetailView>{
    private ContactDetailInteractor contactDetailInteractor;

    @Inject
    public ContactDetailPresenter(ContactDetailInteractor contactDetailInteractor) {
        this.contactDetailInteractor = contactDetailInteractor;
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onInit() {
        super.onInit();
        EventBus.getDefault().register(this);
    }

    void loadContact(int id) {
        contactDetailInteractor.getContact(id);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onContactDetailLoaded(@NonNull FetchContactDetailEventMessage eventMessage) {
        if (eventMessage.contact != null) {
            view.contactLoaded(eventMessage.contact);
        }
    }
}
