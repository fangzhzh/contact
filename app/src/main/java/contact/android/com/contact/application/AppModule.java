package contact.android.com.contact.application;

import android.app.Application;
import android.content.res.Resources;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import javax.inject.Singleton;

import contact.android.com.contact.api.ContactsAPIService;
import contact.android.com.contact.database.DatabaseManager;
import contact.android.com.contact.utils.CONST;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author zhangzhenfang
 * @since 1/18/17 4:33 AM
 */
@Module
public class AppModule {
    private Application application;
    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return  application;
    }

    @Provides
    @Singleton
    public Resources provideResources() {
        return application.getResources();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit() {
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        return new Retrofit.Builder()
                .baseUrl(CONST.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okClient)
                .build();
    }
    @Provides
    @Singleton
    public ContactsAPIService provideContactsAPIService() {
        return provideRetrofit().create(ContactsAPIService.class);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    public DatabaseManager provideDatabaseManager() {
        return new DatabaseManager(application);
    }
}
