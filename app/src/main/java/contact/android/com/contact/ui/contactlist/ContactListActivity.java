package contact.android.com.contact.ui.contactlist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import contact.android.com.contact.models.ContactWithNote;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.util.List;

import javax.inject.Inject;

import contact.android.com.contact.R;
import contact.android.com.contact.activity.ActivityModule;
import contact.android.com.contact.application.ContactApplication;
import contact.android.com.contact.ui.base.BaseActivity;
import contact.android.com.contact.ui.base.LoadingProgress;
import contact.android.com.contact.ui.base.Scope;
import contact.android.com.contact.utils.CONST;
import contact.android.com.contact.utils.Navigator;
import contact.android.com.contact.utils.SnackbarUtils;

/**
 * @author zhangzhenfang
 * @since 1/18/17 4:33 AM
 */
@EActivity(R.layout.activity_contact_list)
public class ContactListActivity extends BaseActivity implements ContactListView {
    @ViewById(R.id.swipeContainer)
    SwipeRefreshLayout swipeRefreshLayout;

    @ViewById(R.id.contact_list)
    ListView contactLististView;
    @ViewById(R.id.emptyView)
    View emptyView;
    @ViewById(R.id.contact_list_toolbar)
    Toolbar toolbar;

    @Inject
    LoadingProgress progressBar;
    @Inject
    Scope scope;
    @Inject ContacWithNoteListAdapter adapter;
    @Inject
    ContactListPresenter presenter;
    @Inject
    Navigator navigator;

    @StringRes(R.string.ct_loading_contact)
    String loadingContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setupActivityComponent() {
        ContactApplication.get().getAppComponent().plus(new ActivityModule(this)).inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_list_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @AfterViews
    void onViewInit() {
        setSupportActionBar(toolbar);
        scope.attach(presenter);
        presenter.takeView(this);
        contactLististView.setEmptyView(emptyView);
        contactLististView.setAdapter(adapter);
        presenter.getContact();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getContact();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_asc:
                presenter.sortContact(CONST.SORTED_ORDER.asc);
                break;
            case R.id.action_note:
                presenter.sortContact(CONST.SORTED_ORDER.note);
                break;
            case R.id.action_des:
                default:
                presenter.sortContact(CONST.SORTED_ORDER.dec);
                    break;
        }
        return true;
    }

    @Override
    public void contactLoaded(@NonNull List<ContactWithNote> contactList) {
        adapter.setDataSource(contactList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void startLoading() {
        progressBar.show(loadingContact);
    }

    @Override
    public void stopLoading() {
        progressBar.hide();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onError(int errCode, String errMsg) {
        SnackbarUtils.showSnackbar(this, getResources().getString(R.string.loading_error_message, errCode, errMsg));
    }

    @Override
    public void onError(int errCode, int msgId) {
        onError(errCode, getResources().getString(msgId));
    }

    @ItemClick(R.id.contact_list)
    void onItemClick(ContactWithNote contact) {
        navigator.gotoContactDetail(contact);
    }

    @Click(R.id.create_new_contact)
    void onCreateNewContactClick() {
        navigator.gotoCotactNew();
    }

    @OnActivityResult(CONST.CONTACT_NEW_REQUEST_CODE)
    void onContactCreated(int resultCode) {
        if (resultCode == RESULT_OK) {
            presenter.getContact();
        } else {
            onError(-1, getResources().getString(R.string.error_saving_contact));
        }
    }
}
