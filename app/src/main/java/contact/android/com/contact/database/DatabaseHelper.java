package contact.android.com.contact.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;

import contact.android.com.contact.database.dao.BaseInfoDao;
import contact.android.com.contact.utils.CONST;

/**
 * @author zhangzhenfang
 * @since 1/19/17 5:57 PM
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    // private members
    private final DatabaseManager mDatabaseManager;
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context, String databaseName,DatabaseManager manager) {
        super(context, databaseName, null, DATABASE_VERSION);
        mDatabaseManager = manager;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            createTables();
        }
        catch(SQLException e){
            Log.e(CONST.LOG_TAG, String.format("%s %s %s", DatabaseHelper.class.getName(), "can't create database", e.toString()));
            throw new RuntimeException(e);
        }
    }

    public void init() {
        getReadableDatabase();
    }

    private void createTables() throws SQLException {
        HashMap<String, BaseInfoDao> daoList = mDatabaseManager.getDaoMap();
        for (String key : daoList.keySet()) {
            Class ormBeanClass = daoList.get(key).getDBObjectClass();
            TableUtils.createTableIfNotExists(connectionSource, ormBeanClass);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        Log.i(CONST.LOG_TAG, "Database upgrade call back");
        if (newVersion > oldVersion) {
            UpgradeHelper helper =  new UpgradeHelper(database, oldVersion, newVersion);
            helper.runAllUpgrades();
        }
    }

}
