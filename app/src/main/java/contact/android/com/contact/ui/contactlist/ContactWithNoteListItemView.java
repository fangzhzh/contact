package contact.android.com.contact.ui.contactlist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;
import contact.android.com.contact.R;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.ContactWithNote;
import contact.android.com.contact.ui.base.IListItemView;
import contact.android.com.contact.utils.ListUtils;
import contact.android.com.contact.utils.TimeUtil;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * @author zhangzhenfang
 * @since 1/18/17 12:17 PM
 */
@EViewGroup(R.layout.contact_list_item)
public class ContactWithNoteListItemView extends FrameLayout implements IListItemView<ContactWithNote>{
    @ViewById(R.id.contact_list_item_name)
    TextView name;
    @ViewById(R.id.contact_list_item_phone)
    TextView phone;
    @ViewById(R.id.contact_list_item_note)
    TextView lastNote;


    public ContactWithNoteListItemView(Context context) {
        super(context);
    }

    public ContactWithNoteListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bind(ContactWithNote data) {
        name.setText(data.name + ", " + data.secondName);
        phone.setText(data.phone);
        if (ListUtils.isEmpty(data.note)) {
            lastNote.setVisibility(GONE);
        } else {
            lastNote.setVisibility(VISIBLE);
            lastNote.setText(data.note.get(0).getNote() + " @ " + TimeUtil.getReadableDate(data.note.get(0).getTimestamp()));
        }
    }
}
