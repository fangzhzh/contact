package contact.android.com.contact.ui.contactlist;

import contact.android.com.contact.models.ContactWithNote;
import java.util.List;

import contact.android.com.contact.models.Contact;

/**
 * @author zhangzhenfang
 * @since 1/18/17 2:03 PM
 */
public interface ContactListView {
    void contactLoaded(List<ContactWithNote> contactList);
    void startLoading();
    void stopLoading();
    void onError(int errCode, String errMsg);
    void onError(int errCode, int msgId);
}
