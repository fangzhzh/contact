package contact.android.com.contact.utils;

import android.app.Activity;
import android.content.Context;

import contact.android.com.contact.models.ContactWithNote;
import contact.android.com.contact.ui.contactnew.ContactNewActivity_;
import contact.android.com.contact.ui.contactwithnotedetail.ContactWithNoteDetailActivity_;
import javax.inject.Inject;



/**
 * @author zhangzhenfang
 * @since 1/19/17 3:16 PM
 */
public class Navigator {
    private Activity activity;

    @Inject
    public Navigator(Activity activity) {
        this.activity = activity;
    }

    public Context getContext() {
        return this.activity;
    }

    public void gotoContactDetail(ContactWithNote contact) {
        ContactWithNoteDetailActivity_
                .intent(activity)
                .contactId(contact.id)
                .start();
    }

    public void gotoCotactNew() {
        ContactNewActivity_
            .intent(activity)
            .startForResult(CONST.CONTACT_NEW_REQUEST_CODE);
    }
}
