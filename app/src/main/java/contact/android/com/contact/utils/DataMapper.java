package contact.android.com.contact.utils;

import com.google.gson.Gson;

import contact.android.com.contact.models.ContactWithNote;
import javax.inject.Inject;

import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.Address;
import contact.android.com.contact.models.Company;
import contact.android.com.contact.models.Contact;

/**
 * @author zhangzhenfang
 * @since 1/19/17 7:12 PM
 */
public class DataMapper {
    private Gson gson;

    @Inject
    public DataMapper(Gson gson) {
        this.gson = gson;
    }

    public void map(final Contact protoObject, DBContact dbContact) {
        dbContact.setId(protoObject.id);
        dbContact.setName(protoObject.name);
        //dbContact.setUsername(protoObject.username);
        //dbContact.setEmail(protoObject.email);
        dbContact.setPhone(protoObject.phone);
        //dbContact.setWebsite(protoObject.website);
        //dbContact.setAddress(gson.toJson(protoObject.address));
        //dbContact.setCompany(gson.toJson(protoObject.company));
    }


    public Contact map(DBContact dbContact) {
        Contact contact = new Contact(dbContact.getId(), dbContact.getName(),
                "", "", null, dbContact.getPhone(),
                null, null);
        return contact;
    }


    public ContactWithNote mapContactWithNote(DBContact dbContact) {
        ContactWithNote contact = new ContactWithNote(dbContact.getId(), dbContact.getName(),
            dbContact.getSecondName(),  dbContact.getPhone(),
            dbContact.getNote());
        return contact;
    }

}
