package contact.android.com.contact.activity;

import android.app.Activity;
import android.content.Context;

import contact.android.com.contact.ui.base.BaseActivity;
import contact.android.com.contact.ui.base.Scope;
import contact.android.com.contact.utils.Navigator;
import contact.android.com.contact.utils.UILoop;
import dagger.Module;
import dagger.Provides;

/**
 * @author zhangzhenfang
 * @since 1/18/17 4:33 AM
 */
@Module
public class ActivityModule {
    private BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity provideActivity() {
        return activity;
    }

    @ActivityScope
    @Provides
    Context provideContext() {
        return activity;
    }


    @ActivityScope
    @Provides
    Scope provideScope() {
        return activity;
    }

    @Provides
    @ActivityScope
    UILoop provideUILoop() {
        return new UILoop();
    }

    @ActivityScope
    @Provides
    Navigator provideNavigator() {
        return new Navigator(activity);
    }

}
