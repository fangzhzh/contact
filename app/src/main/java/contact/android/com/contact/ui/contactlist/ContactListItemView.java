package contact.android.com.contact.ui.contactlist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import contact.android.com.contact.R;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.ui.base.IListItemView;

/**
 * @author zhangzhenfang
 * @since 1/18/17 12:17 PM
 */
@EViewGroup(R.layout.contact_list_item)
public class ContactListItemView extends FrameLayout implements IListItemView<Contact>{
    @ViewById(R.id.contact_list_item_name)
    TextView name;
    @ViewById(R.id.contact_list_item_phone)
    TextView phone;


    public ContactListItemView(Context context) {
        super(context);
    }

    public ContactListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bind(Contact data) {
        name.setText(data.name);
        phone.setText(data.phone);
    }
}
