package contact.android.com.contact.ui.contactwithnotedetail;

import contact.android.com.contact.models.ContactWithNote;

/**
 * @author zhangzhenfang
 * @since 1/19/17 8:26 PM
 */
public interface ContactWithNoteDetailView {
    void contactLoaded(ContactWithNote contact);
}
