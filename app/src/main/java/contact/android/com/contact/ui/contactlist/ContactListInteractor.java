package contact.android.com.contact.ui.contactlist;


import contact.android.com.contact.models.ContactWithNote;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import contact.android.com.contact.api.ContactsAPIService;
import contact.android.com.contact.database.orm.DBContact;
import contact.android.com.contact.models.Contact;
import contact.android.com.contact.models.store.ContactStore;
import contact.android.com.contact.ui.base.BaseInteractor;
import contact.android.com.contact.utils.DataMapper;
import contact.android.com.contact.utils.ListUtils;
import dagger.Lazy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author zhangzhenfang
 * @since 1/18/17 3:45 PM
 */

public class ContactListInteractor extends BaseInteractor<ContactListInteractor.ContactListData> {

    private Lazy<ContactsAPIService> contactsAPIService;
    private Lazy<DataMapper> dataMapper;
    private Lazy<ContactStore> contactStore;

    @Inject
    public ContactListInteractor(Lazy<ContactsAPIService> contactsAPIService,
                                 Lazy<DataMapper> dataMapper,
                                 Lazy<ContactStore> contactStore) {
        this.contactsAPIService = contactsAPIService;
        this.dataMapper = dataMapper;
        this.contactStore = contactStore;
    }

    public void getContacts() {
        ContactListData data = new ContactListData();
        execute(data);
    }

    @Override
    protected void onExecute(final ContactListData data) {

        List<DBContact> dbContactList = contactStore.get().getAllContacts();
        List<ContactWithNote> contactList = new ArrayList<>();
        if (!ListUtils.isEmpty(dbContactList)) {
            for (DBContact dbContact : dbContactList) {
                ContactWithNote contact = dataMapper.get().mapContactWithNote(dbContact);
                contactList.add(contact);
            }
            EventBus.getDefault().post(new FetchContactEventMessage(contactList, 0, ""));
        }

    }

    public static class ContactListData extends BaseInteractor.Data {

        public ContactListData() {
            super("ContactListInteractor", "ContactListInteractor");
        }
    }
}
