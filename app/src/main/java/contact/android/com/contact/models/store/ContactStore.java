package contact.android.com.contact.models.store;

import java.util.List;

import javax.inject.Inject;

import contact.android.com.contact.database.DatabaseManager;
import contact.android.com.contact.database.dao.ContactDao;
import contact.android.com.contact.database.orm.DBContact;

/**
 * @author zhangzhenfang
 * @since 1/19/17 7:19 PM
 */
public class ContactStore {
    private ContactDao contactDao;

    @Inject
    public ContactStore(DatabaseManager databaseManager) {
        contactDao = databaseManager.getContactDao();
    }

    public void saveContact(DBContact contact) {
        contactDao.save(contact);
    }

    public void saveContactList(List<DBContact> dbContactList) {
        contactDao.save(dbContactList);
    }

    public DBContact getContact(int contactId) {
        return contactDao.get(contactId);
    }

    public List<DBContact> getAllContacts() {
        return contactDao.getAll();
    }

    public void removeAll() {
        contactDao.removeAll();
    }
}