package contact.android.com.contact.database;

import android.content.Context;
import com.j256.ormlite.dao.DaoManager;
import contact.android.com.contact.database.dao.BaseInfoDao;
import contact.android.com.contact.database.dao.ContactDao;
import contact.android.com.contact.database.dao.NoteDao;
import java.util.HashMap;

/**
 * @author zhangzhenfang
 * @since 1/19/17 5:54 PM
 */
public class DatabaseManager {
    private DatabaseHelper mDBHelper;
    private HashMap<String, BaseInfoDao> mDaoList;
    private static final String ContactDao = "ContactDao";
    private static final String NOTE_DAO = "NoteDao";

    public HashMap<String, BaseInfoDao> getDaoMap() {
        return mDaoList;
    }

    public DatabaseManager(Context context) {
        initDB(context);
    }

    private void initDB(Context context) {
        // lets open the database and read it.

        mDBHelper = new DatabaseHelper(context.getApplicationContext(), "Contact.db", this);

        // let us initialize the application DAO objects
        initDao();
        // only now init the helper.
        mDBHelper.init();
    }

    private void initDao() {
        mDaoList = new HashMap<>();
        registerDao(ContactDao, new ContactDao(mDBHelper));
        registerDao(NOTE_DAO, new NoteDao(mDBHelper));
    }

    public void registerDao(String daoName, BaseInfoDao dao) {
        mDaoList.put(daoName, dao);
    }

    public ContactDao getContactDao() {
        return (ContactDao) getDaoMap().get(ContactDao);
    }

    public NoteDao getNoteDao() {
        return (NoteDao) getDaoMap().get(NOTE_DAO);
    }

    public void clearCache() {

        if (mDBHelper == null) {
            return;
        }

        for (BaseInfoDao dao : mDaoList.values()) {
            dao.clearCache();
        }
        mDaoList.clear();
        mDBHelper.close();

        DaoManager.clearCache();
        DaoManager.clearDaoCache();
    }
}
